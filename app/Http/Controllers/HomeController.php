<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;
use App\Http\Resources\SearchResultCollection;
use Illuminate\Pagination\LengthAwarePaginator;

class HomeController extends Controller
{

     private function searchValidation() {
      return [
            'search' => 'required',
        
        ];
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //authintication here not neccessary
        // $this->middleware('auth');
    }

//         private $url='https://google-search5.p.rapidapi.com/google-serps/';
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    } 

    public function search(Request $request)
    {
         $validatedData = $request->validate($this->searchValidation());
         //call search api
        $response = Http::withHeaders(config('constants.header'))->GET(config('constants.url'),[
        'q'=>$validatedData['search'],
        ]);
        $res=collect($response['data']['results']['organic']);
        //pagination
       $page = request()->get('page');
       $perPage = 10;
       $paginator = new LengthAwarePaginator(
       $res->forPage($page, $perPage), $res->count(), $perPage, $page
        );
    $total=$res->count();
    //count of page in pagination because make it manually
    $pageCount=ceil($total/$perPage);
    //set result to collection
       $result= SearchResultCollection::make($paginator);
         return view('search',compact('result','pageCount'));

    }
    
}
