@extends('layouts.app')

@section('content')

<div class="grid-container">
          @foreach($result->collection as $ser)
<div class="grid-item">
          <a target="_blank" href="{{$ser['url']}}" >
  <img src="../assets/images/slider.jpeg" alt="Forest" style="width:250px;margin-left: 100px;">
</a>
<h3>{{$ser['title']}}</h3>
</div>
          @endforeach

</div>
<!-- {!! $result->render() !!} -->

<div aria-label="Page navigation example" class="mb-4">
              <ul class="custom-pagination pagination-info pagination align-items-stretch">
                <li class="page-item" onclick="">
                  <a class="page-link" href="/search?page=1" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                  </a>
                </li>
              @for ($i = 1; $i <= $pageCount; $i++)
                <li class="page-item active"><a class="page-link" href="#">{{$i}}</a></li>
              @endfor
                <li class="page-item">
                  <a class="page-link" href="/search?page={{$pageCount}}" aria-label="Next">
                    <span aria-hidden="true">&raquo;</span>
                  </a>
                </li>
              </ul>
            </div>

@endsection