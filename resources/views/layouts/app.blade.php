<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script type="text/javascript" src="{{ asset('assets/js/script.bundle.js') }}"></script>
    <!-- when auth redirect to login modal -->
@if(session('login'))
    <script type="text/javascript">
      $(function() {
        $('.cd-user-modal').addClass('is-visible');
        $('.cd-switcher').children('li').eq(0).children('a').addClass('selected');
        $('.cd-user-modal').find('#cd-login').addClass('is-selected');
      });
    </script>
@endif
   <link rel="preconnect" href="https://fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css2?family=Josefin+Sans:wght@100;400&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/4.1.1/animate.min.css"/>
    <link href="{{ asset('assets/css/style.bundle.css') }}" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
</head>
<body>
<header>
    
    <nav>
        <div class="logo"><h1 class="animate__animated animate__heartBeat">ACME SOFTWARE</h1></div>
        <div class="menu">
           <a href="#">Home</a>
           <a href="#">About us</a>
           <a href="#">Contact</a>
            @guest
            <a href="#" class="sign_up_btn">Sign up</a>

                @else
                <div class="dropdown">
    <a class="dropbtn">{{ auth()->user()->name }} 
      <i class="fa fa-caret-down"></i></a>
    <div class="dropdown-content">
     <li id="menu-item-2368" class="color-2">
                                        <a href="#" onclick="event.preventDefault();
                                           document.getElementById('logout-form').submit();" class="has-icon">logout
                                            </a>
                                    </li>
                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
    </div>
               

            @endguest
            
        </div>
</nav>
</header>
            @yield('content')
    

    <div class="cd-user-modal" id="cd-user-modal"> <!-- this is the entire modal form-->
        <div class="cd-user-modal-container"> 
            <ul class="cd-switcher">
                <li><a href="#0">Sign in</a></li>
                <li><a href="#0">New account</a></li>
            </ul>

            <div id="cd-login"> <!-- log in form -->

                <form class="cd-form" action="{{route('login')}}" method="POST">
                    @csrf
                    <p class="fieldset">
                        <label class="image-replace cd-email" for="signin-email" >E-mail</label>
                        <input class="full-width has-padding has-border" id="signin-email" type="email" placeholder="E-mail" name="email">
                        <span class="cd-error-message">Error message here!</span>
                    </p>

                    <p class="fieldset">
                        <label class="image-replace cd-password" for="signin-password">Password</label>
                        <input class="full-width has-padding has-border" id="signin-password" type="text"  placeholder="Password" name="password">
                        <a href="#0" class="hide-password">Hide</a>
                        <span class="cd-error-message">Error message here!</span>
                    </p>

                    <p class="fieldset">
                        <input type="checkbox" id="remember-me" checked>
                        <label for="remember-me">Remember me</label>
                    </p>

                    <p class="fieldset">
                        <button  class="full-width" type="submit" >Login</button>
                    </p>

                    <!-- if  sign up successfully show success message  -->
                    @if(session('success') )
                          <div class="div-success">{{session('success')}} </div>
                    @endif

                    @if(session('error_signin') )
                          <div class="div-danger">{{session('error_signin')}} </div>
                    @endif
                </form>
                
                <p class="cd-form-bottom-message"><a href="#0">Forgot your password?</a></p>
                <!-- <a href="#0" class="cd-close-form">Close</a> -->
            </div> <!-- cd-login -->

            <div id="cd-signup"> <!-- sign up form -->
                <form class="cd-form" action="{{ route('register') }}" method="POST">
                   @csrf
                    <p class="fieldset">
                        <label class="image-replace cd-username" for="signup-username">Username</label>
                        <input class="full-width has-padding has-border" id="signup-username" type="text" placeholder="Username" name="name">
                        <span class="cd-error-message">Error message here!</span>
                    </p>

                    <p class="fieldset">
                        <label class="image-replace cd-email" for="signup-email">E-mail</label>
                        <input class="full-width has-padding has-border" id="signup-email" type="email" placeholder="E-mail" name="email">
                        <span class="cd-error-message">Error message here!</span>
                    </p>

                    <p class="fieldset">
                        <label class="image-replace cd-password" for="signup-password">Password</label>
                        <input class="full-width has-padding has-border" id="signup-password" type="text"  placeholder="Password" name="password">
                        <a href="#0" class="hide-password">Hide</a>
                        <span class="cd-error-message">Error message here!</span>
                    </p>

                     <p class="fieldset">
                        <label class="image-replace cd-password" for="signup-password">Confirm Password</label>
                        <input class="full-width has-padding has-border" id="signup-password" type="text"  placeholder="password confirm" name="password_confirmation">
                        <a href="#0" class="hide-password">Hide</a>
                        <span class="cd-error-message">Error message here!</span>
                    </p>


                    <p class="fieldset">
                        <input type="checkbox" id="accept-terms">
                        <label for="accept-terms">I agree to the <a href="#0">Terms</a></label>
                    </p>

                    <p class="fieldset">
                        <button class="full-width has-padding" type="submit" >Creat Account</button>
                    </p>

                    <!-- if error in  sign up show error message  -->
                    @if(session('error') )
                          <div class="div-danger">{{session('error')}} </div>
                    @endif
                </form>

                <!-- <a href="#0" class="cd-close-form">Close</a> -->
            </div> <!-- cd-signup -->

            <div id="cd-reset-password"> <!-- reset password form -->
                <p class="cd-form-message">Lost your password? Please enter your email address. You will receive a link to create a new password.</p>

                <form class="cd-form">
                    <p class="fieldset">
                        <label class="image-replace cd-email" for="reset-email">E-mail</label>
                        <input class="full-width has-padding has-border" id="reset-email" type="email" placeholder="E-mail">
                        <span class="cd-error-message">Error message here!</span>
                    </p>

                    <p class="fieldset">
                        <button class="full-width has-padding" type="submit" >Reset Password</button>
                    </p>
                </form>

                <p class="cd-form-bottom-message"><a href="#0">Back to log-in</a></p>
            </div> <!-- cd-reset-password -->
            <a href="#0" class="cd-close-form">Close</a>
        </div> <!-- cd-user-modal-container -->
    </div> <!-- cd-user-modal -->
</body>
</html>
