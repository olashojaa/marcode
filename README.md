##This is test project use Laravel 7.x

## Quick Installation

    git clone https://olashojaa@bitbucket.org/olashojaa/marcode.git

    cd quickstart

    composer install

    php artisan migrate

    php artisan serve


##Notes

   This website is emplement using html5 and css3 without bootstrap.And has some simple animation.

   It has authintication system that connect to local database.

   It also has search form in home page that call test external api from google and get data and display it into view as thumbnails.In addition to use pagination for better performance.
   
   Finally, you can change api provider from constant file.
 
